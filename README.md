# Technical bootcamp 3 
## Informace o kurzu:
- Ucastnik Technical Bootcamp 3 si samostatne nascriptuje 5 test casu z preddefinovanych 9 (viz nize) v Robot Framework + Browser library. Po odevzdani reseni si naplanuje schuzku s lektorem, behem niz si kod spolecne projdou. Vystupem budou poznamky slouzici pro zlepseni autoamtizacnich dovednosti a kvality kodu dle predem definovanych metrik.
- Tento kurz navazuje na predchozi Technical Bootcamps. Ucastnik po jeho absolvovani bude schopen dat dohromady frontend testy pomoci Robot Framework.
## Softwarove pozadavky:
- Alespon zakladni znalost Robot Framework (Tedy umet rozhybat browser a napsat test case/vlastni keyword. Pro pripadny refresh doporucuju nejaky z kurzu na Tesena uctu udemy.com, napr [Robot Framework Level 1](https://www.udemy.com/course/robot-framework-level-1/) - prestoze se tam pro automatizaci browseru misto Browser library pouziva Selenium library)
- [Python](https://www.python.org/downloads/release/python-379/)
- [IDE](https://www.jetbrains.com/pycharm/download/#section=windows) - napr. PyCharm Community edition, nebo VS Code
- IntelliBot/Robot Framework language server plugin na smart editing v pripade Pycharm IDE / Robot Framework Intellisense v pripade VS Code. V Pycharm je mozne dostat se do instalace pluginu pomoci CTRL+ALT+S (Settings) -> Plugins -> Marketplace tab. Po instalaci a restartu IDE je mozne dalsi nastaveni najit v Settings -> Languages & Frameworks -> Robot Framework (Global)/(Project). Ve VS Code je mozne se pomoci CTRL+SHIFT+X dostat do Extensions tabu, kde uz samotny plugin staci vyhledat a nainstalovat. Fungovat by mel okamzite.
- Robot Framework: `pip install robotframework`
- [node.js](https://nodejs.org/en/download/)
- [Browser Library:](https://robotframework-browser.org/) `pip install robotframework-browser`
- [GIT](https://git-scm.com/downloads) 
- rozbehnout browser library pomoci: `rfbrowser init`
## Ukol
- Navrhnout a nascriptovat testy na strance http://tutorialsninja.com/demo/ (jednoduchy testovaci e-shop) pomoci robot framework a browser library. Testy mohou byt pozitivni/negativni, fantazii se meze nekladou a zadny pristup neni spatne, kdyz je dobre oduvodneny. Zadani nize. Muze byt i vice testu na scenar.
- Vytvorit readme.md soubor/okomentovat kod tak, aby byly popsane jednotlive testy a jejich cile. Pro splneni je potreba alespon 5 nascriptovanych testu (celkem) na alespon 5 scenaru ze zadani (viz nize).
- Odevzdat reseni jako git repository (github, gitlab etc.)
- V repozitari najdete vzorovy priklad
- Pred zacatkem doporucuji podivat se na demo video v Doplnkovych materialech.
### Zadani testovacich scenaru:
1) Zaregistrovat se a ulozit si pouzity e-mail/password do .csv souboru (pokud pozitivni scenar).
   Pro ucely testu predpokladejme, ze:
   - format e-mailove adresy musi byt x@y.z (5 znaku minimum)
   - telefon - alespon 3 znaky (nemusi nutne byt cislice)
   - heslo - alespon 4 znaky
   - Privacy policy agreement je povinny
   - Pri chybe se zobrazi error message
2) Login (idealne pomoci e-mailu a hesla z .csv souboru)
   Pro ucely testu predpokladejme, ze: 
   - Musi byt vyplnene existujici e-mail a spravna kombinace paru e-mail + heslo
   - Pri chybe se zobrazi error message
3) Vlozit zbozi do kosiku a nasledne ho odstranit z kosiku
4) Napsat review na zbozi
   Pro ucely testu predpokladejme, ze:
   - Name musi byt vyplneno
   - Review musi mit alespon 25 znaku  
   - Po odeslani review se zobrazi success message
   - Pri chybe se zobrazi error message
5) V nejake z kategorii zbozi (Desktops, Laptops & notebooks...) overit funkcionalitu "Sort By" (staci si vybrat jeden, napr. price from highest to lowest)
6) Product comparison (zbozi je mozno porovnavat mezi sebou - k nalezeni vedle buttons "ADD TO CART" a "Add to Wish List")
7) Overeni poctu polozek v kategorii (napr. v desktops ma byt prave 12 produktu, v laptops & notebooks prave 5 produktu etc...)
8) Vyhledani zbozi
9) Pridat zbozi do wishlistu a nasledne ho odstranit z wishlistu
## Na co si dat pozor: 
- Variable naming convention
- Duplikace kodu (dobre strukturovany projekt - page objects)
- Dobre osetrovat chyby (custom error message)
- Kvalitni dokumentace kodu
- Assert navigace na strance
- Pouzivat unikatni lokatory
- [Klikni zde pro vice informaci o best practices](https://github.com/robotframework/HowToWriteGoodTestCases/blob/master/HowToWriteGoodTestCases.rst)
